Note that Bitbucket has a `gist-like facility <https://bitbucket.org/blog/snippets-for-teams-are-here-with-a-rich-set-of-apis>`_ called *Snippets*, but I started this before that existed.

TODOs
+++++

1. Describe contents: give thumbnails in this README
2. Migrate to `my snippets <https://bitbucket.org/dashboard/snippets>`_
