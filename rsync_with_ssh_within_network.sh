# transfers augmented (and currently un-ASM-ed) BCs from EMVL to cwd on HPCC
# almost same code as i:/project/inf35w/me/IC/2008cdc/AQplusN2O/get.sh : TODO: make driver for paths
# Used to rsync from terrae to infinity, run on infinity as `./get.sh`:

# me@infinity:/project/inf35w/me/BC/2008cdc/AQplusN2O $ ./get.sh
# > START=Wed Aug 28 19:23:44 EDT 2013
# > $ rsync -avh --stats --append -e ssh me@terrae.nesc.epa.gov:/work/MOD3APP/me/BC/2008cdc/AQplusN2O/ ./
# > receiving file list ... done
# > ./
# > BCON_CB05AE5_US12_2007356
# > BCON_CB05AE5_US12_2007357
# ...
# > BCON_CB05AE5_US12_2008365
# > BCON_CB05AE5_US12_2008366
# >
# > Number of files: 377
# > Number of files transferred: 376
# > Total file size: 123.47G bytes
# > Total transferred file size: 123.47G bytes
# > Literal data: 123.47G bytes
# > Matched data: 0 bytes
# > File list size: 4644
# > File list generation time: 1.622 seconds
# > File list transfer time: 0.000 seconds
# > Total bytes sent: 8.30K
# > Total bytes received: 123.48G
# >
# > sent 8.30K bytes  received 123.48G bytes  45.12M bytes/sec
# > total size is 123.47G  speedup is 1.00
# > after transfer:
# >   END=Wed Aug 28 20:09:20 EDT 2013
# > START=Wed Aug 28 19:23:44 EDT 2013

EMVL_USER='me' # replace
EMVL_HOST='terrae.nesc.epa.gov' # the gateway
EMVL_SOURCE_DIR='/work/MOD3APP/rtd/BC/2008cdc/AQplusN2O'
HPCC_TARGET_DIR='.'

EMVL_SOURCE_FQP="${EMVL_USER}@${EMVL_HOST}:${EMVL_SOURCE_DIR}/" # TODO: better ensure trailing slash
HPCC_TARGET_PATH="${HPCC_TARGET_DIR}/"  # TODO: better ensure trailing slash
# RSYNC_OPTIONS='-avh --stats --delete-after --append-verify -e ssh'
# infinity has `rsync --version`==2.6.8, knows no '--append-verify'
# RSYNC_OPTIONS='-avh --stats --delete-after --append -e ssh'
# --delete* deletes this script!
RSYNC_OPTIONS='-avh --stats --append -e ssh'

START="$(date)"
echo -e "START=${START}"

if [[ ! -z "${HPCC_TARGET_DIR}" ]] ; then
  if [[ -r "${HPCC_TARGET_DIR}" ]] ; then
    for CMD in \
      "rsync ${RSYNC_OPTIONS} ${EMVL_SOURCE_FQP} ${HPCC_TARGET_PATH}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  else
    echo -e "ERROR: cannot read target dir='${HPCC_TARGET_DIR}'"
  fi # [[ -r "${HPCC_TARGET_DIR}" ]]
else
  echo -e 'ERROR: HPCC_TARGET_DIR not defined'
fi # [[ ! -z "${HPCC_TARGET_DIR}" ]]

echo -e 'after transfer:' # TODO: include ${THIS}
# TODO: compute source and target stats: n(files) , du(files) , etc
# for CMD in \
#   "" \
# ; do
#   echo -e "$ ${CMD}"
# #  eval "${CMD}"
# done

echo -e "  END=$(date)"
echo -e "START=${START}"
