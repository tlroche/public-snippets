# UNTESTED: checkout code with given branch from git repo:
# not applicable to CMAQ-5.x, which tags

REPO_NAME='CCTM'
BRANCH_NAME='whatever'
REPO_HOST='terrae' # let it choose login node
REPO_PATH="/home/yoj/src/repo/${REPO_NAME}/${REPO_NAME}.git" # FQP on terrae
REPO_URI="${REPO_PATH}" # if checking out on terrae, from terrae repo
# REPO_URI="ssh://${REPO_HOST}${REPO_PATH}" # if checking out on infinity, from terrae repo

#   "ls -alh ${REPO_PATH}" \ # for debugging on terrae--see below
for CMD in \
  "git init" \
  "git remote add -t ${BRANCH_NAME} -f origin ${REPO_URI}" \
  "git checkout ${BRANCH_NAME}" \
  "ls -alh" \
; do
  echo -e "$ ${CMD}"
  eval "${CMD}"
done

# note currently fails to checkout *from terrae* due to permissions problems
# $ ls -alh /home/yoj/src/repo/CCTM/CCTM.git
# ls: /home/yoj/src/repo/CCTM/CCTM.git: Permission denied
# $ git clone /home/yoj/src/repo/CCTM/CCTM.git
# fatal: repository '/home/yoj/src/repo/CCTM/CCTM.git' does not exist

# and also

# me@terraX:~ $ ls -ald /home/yoj/src/repo/
# > drwxr-xr-x 30 yoj mod3dev 8192 Aug 29 07:20 /home/yoj/src/repo/
# me@terraX:~ $ ls -ald /home/yoj/src/repo/CCTM/
# > drwxr-x--- 27 yoj mod3dev 8192 Aug 29 08:15 /home/yoj/src/repo/CCTM/
# me@terraX:~ $ ls -alh /home/yoj/src/repo/CCTM/
# > ls: /home/yoj/src/repo/CCTM/: Permission denied
# me@terraX:~ $ groups
# > mod3app
